package com.swagger.swagger.service;

import com.swagger.swagger.entity.Course;
import com.swagger.swagger.entity.Student;
import com.swagger.swagger.repository.CourseRepository;
import com.swagger.swagger.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private CourseRepository courseRepository;


    public Student updateStudent(int student_id, Student studentDetails){
        Student updateStudent = studentRepository.findById((long) student_id).orElseThrow();
        updateStudent.setStudentId(studentDetails.getStudentId());
        updateStudent.setFirstName(studentDetails.getFirstName());
        updateStudent.setLastName(studentDetails.getLastName());
        updateStudent.setEmail(studentDetails.getEmail());
        updateStudent.setBirthday(studentDetails.getBirthday());
        updateStudent.setLevel(studentDetails.getBirthday());

        return studentRepository.save(updateStudent);
    }

    public void enrollStudentInCourse (int student_id, int course_id){
        Student student = studentRepository.findById((long)student_id).orElse(null);
        Course course = courseRepository.findById((long)course_id).orElse(null);
        student.getCourse().add(course);
        studentRepository.save(student);
    }
    public void dropStudentInCourse (int student_id,int course_id){
        Student student = studentRepository.findById((long)student_id).orElse(null);
        Course course = courseRepository.findById((long)course_id).orElse(null);
        student.getCourse().remove(course);
        studentRepository.save(student);

    }
}
