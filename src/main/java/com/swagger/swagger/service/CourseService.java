package com.swagger.swagger.service;

import com.swagger.swagger.entity.Course;
import com.swagger.swagger.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public Course updateCourse(int course_id, Course courseDetails){
        Course updateCourse = courseRepository.findById((long) course_id).orElseThrow();
        updateCourse.setCourseId(courseDetails.getCourseId());
        updateCourse.setCourseName(courseDetails.getCourseName());
        updateCourse.setCourseDescription(courseDetails.getCourseDescription());
        return courseRepository.save(updateCourse);
    }

}
