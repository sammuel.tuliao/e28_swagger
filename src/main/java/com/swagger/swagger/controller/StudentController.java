package com.swagger.swagger.controller;

import com.swagger.swagger.entity.Student;
import com.swagger.swagger.repository.StudentRepository;
import com.swagger.swagger.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/school")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentService studentService;
    //Add Student.
    @PostMapping("/add-student")
    public ResponseEntity<Student> addStudents (Student students){
        return new ResponseEntity<>(studentRepository.save(students), HttpStatus.OK) ;
    }
    //Get All Student.
    @GetMapping("/student-list")
    public ResponseEntity<Iterable<Student>> getAllStudent (){
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }
    //Get Student by ID.
    @GetMapping("/student-info")
    public ResponseEntity<List<Student>> getById (@RequestParam int student_id){
        return new ResponseEntity<>(studentRepository.findBystudentId(student_id), HttpStatus.OK);
    }
    //Delete Student by ID.
    @DeleteMapping("/delete-student")
    public ResponseEntity<List<Student>> deleteById (@RequestParam int student_id){
         if(studentRepository.findBystudentId(student_id).size() > 0 ){
             studentRepository.deleteById((long) student_id);
            return new ResponseEntity<>(HttpStatus.OK);
         }else{
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
    }
    //Update Student by ID.
    @PutMapping("/update-student")
    public ResponseEntity<Student> updateStudent (@RequestParam int student_id,@RequestBody Student studentDetails){
        return new ResponseEntity<>(studentService.updateStudent(student_id,studentDetails),HttpStatus.OK);

    }

    //Enroll a Student to a Course
    @PostMapping("/enroll-student")
    public ResponseEntity<Student> enrollStudent (@RequestParam int student_id,@RequestParam int course_id){
        studentService.enrollStudentInCourse(student_id,course_id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    //Drop a Student from a Course
    @PostMapping("/drop-student")
    public ResponseEntity<Student> dropStudent (@RequestParam int student_id,@RequestParam int course_id){
        studentService.dropStudentInCourse(student_id,course_id);
        return new ResponseEntity<>(HttpStatus.OK);
    }






}
