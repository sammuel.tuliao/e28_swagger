package com.swagger.swagger.controller;
import com.swagger.swagger.entity.Course;
import com.swagger.swagger.repository.CourseRepository;
import com.swagger.swagger.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/school")
public class CourseController {
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseService courseService;

    //Add Course
    @PostMapping("/add-course")
    public ResponseEntity<Course> addStudents (Course courses){
        return new ResponseEntity<>(courseRepository.save(courses), HttpStatus.OK) ;
    }
    //Get All Courses
    @GetMapping("/course-list")
    public ResponseEntity<Iterable<Course>> getAllCourse(){
        return new ResponseEntity<>( courseRepository.findAll(),HttpStatus.OK);
    }
    //Get Course by ID.
    @GetMapping("/course-info")
    public ResponseEntity<Course> getById (@RequestParam int course_id){
        return new ResponseEntity<>(courseRepository.findById((long) course_id).orElseThrow(), HttpStatus.OK);
    }
    //Delete Course by ID.
    @DeleteMapping("/delete-course")
    public ResponseEntity<List<Course>> deleteById (@RequestParam int course_id){
        courseRepository.deleteById((long) course_id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    //Update Course
    @PutMapping("/update-course")
    public ResponseEntity<Course> updateStudent (@RequestParam int course_id,@RequestBody Course courseDetails){
        return new ResponseEntity<>(courseService.updateCourse(course_id,courseDetails),HttpStatus.OK);

    }


}
